#!/bin/bash
# Script to run Veeam Agent For Linux (VAL) backup and email job status
# ===================================================
# = Settings                                        =
# ===================================================
JOBNAME="Job-Name-Here"
SCRIPT_DIR="/etc/veeam/scripts"
LOG_DIR="/var/log/veeam/Backup/$JOBNAME"
JOB_INFO=$(veeamconfig job info --name "$JOBNAME")
EMAIL="systems@example.com"
# ====================================================
# = DO NOT EDIT BELOW THIS LINE!                     =
# ====================================================
if [[ "$(id -u)" -ne 0 ]]; then
    msg="Error: Must be ran as the root user."
    echo $msg
    echo $msg | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
    exit 1
fi
if [[ ! -e "$(command -v mailx)" ]]; then
    msg="Error: mailx is not found or not installed."
    echo $msg
    echo $msg | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
    exit 1
fi
if [[ ! -e "$(command -v veeamconfig)" ]]; then
    msg="Error: veeamconfig is not found or not installed."
    echo $msg
    echo $msg | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
    exit 1
fi
if [[ -z "$JOBNAME" ]]; then
    msg="Error: The JOBNAME ($JOBNAME) setting is empty."
    echo $msg
    echo $msg | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
    exit 1
fi
if [[ ! -d "$SCRIPT_DIR" ]]; then
    msg="Error: The SCRIPT_DIR ($SCRIPT_DIR) setting directory doesn't exist or is not accessible."
    echo $msg
    echo $msg | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
    exit 1
fi
if [[ ! -d "$LOG_DIR" ]]; then
    msg="Error: The LOG_DIR ($LOG_DIR) setting directory doesn't exist or is not accessible."
    echo $msg
    echo $msg | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
    exit 1
fi
if [[ -z "$JOB_INFO" ]]; then
    msg="Error: Unable to fetch job information (veeamconfig job info --name $JOBNAME)."
    echo $msg
    echo  $msg | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
    exit 1
fi

#start backup job
if [[ "$1" = "full" ]]; then
    veeamconfig job start --name "$JOBNAME" --activeFull --retriable --scheduled --highpriority 1> /etc/veeam/scripts/"$JOBNAME".tmp 2> /etc/veeam/scripts/"$JOBNAME".err
else
	veeamconfig job start --name "$JOBNAME" --retriable --scheduled --highpriority 1> /etc/veeam/scripts/"$JOBNAME".tmp 2> /etc/veeam/scripts/"$JOBNAME".err
fi

SESSION_ID=$(grep ID "$SCRIPT_DIR/$JOBNAME.tmp" | awk  '{print $3}' | sed 's/\[//' | sed 's/\]//' | sed 's/\.//')
SESSION_LOG_DIR=$(grep log "$SCRIPT_DIR/$JOBNAME.tmp" | awk  '{print $4}' | sed 's/\[//' | sed 's/\]//' | sed 's/\.//')
LOGFILE="$SESSION_LOG_DIR/Job.log"
# SESSION_INFO=$(veeamconfig session info --id "$SESSION_ID" | grep -v UUID)

# check if another job is running
if [ -s $JOBNAME.err ]
then
    ERROR_MSG=$(grep Error "$SCRIPT_DIR/$JOBNAME.err")
    echo "$ERROR_MSG" | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
fi

#check if job is running
until [ -z "$(pgrep veeamjobman)" ]
do
    echo "job is running" > /dev/null
done

#check and set the exit status of the job
STATUS="$(veeamconfig session info --id $SESSION_ID | grep State | awk  '{print $2}')"

if [[ "$STATUS" = 'Warning' ]]; then
    WARN_MSG=$(veeamconfig session log --id "$SESSION_ID" | grep warn | awk '{print $6,$7,$8,$9}')
    echo -e "Job $JOBNAME Successful with following $STATUS:\n\n$WARN_MSG\n\nJOB INFO:\n$JOB_INFO" | mailx -s "VAL Job $JOBNAME $STATUS" "$EMAIL"
elif [[ "$STATUS" = 'Failed' ]]; then
    echo -e "Job $JOBNAME $STATUS. See attached logfile for error details.\n\nJOB INFO:\n$JOB_INFO" | mailx -s "VAL Job $JOBNAME $STATUS" -a "$LOGFILE" "$EMAIL"
elif [[ "$STATUS" = 'Success' ]]; then
    echo -e "Job $JOBNAME $STATUS.\n\nJOB INFO:\n$JOB_INFO" | mailx -s "VAL Job $JOBNAME $STATUS" "$EMAIL"
else
    echo -e "Job $JOBNAME $STATUS.\n\nJOB INFO:\n$JOB_INFO" | mailx -s "VAL Job $JOBNAME Unknown" "$EMAIL"
fi

rm -f $SCRIPT_DIR/$JOBNAME.tmp $SCRIPT_DIR/$JOBNAME.err || echo "Error: Unable to remove temproary files, '$SCRIPT_DIR/$JOBNAME.tmp' and '$SCRIPT_DIR/$JOBNAME.err'." | mailx -s "VAL Job $JOBNAME Error" "$EMAIL"
