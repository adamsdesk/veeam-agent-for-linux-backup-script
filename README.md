# Veeam Agent Configuration

Configurations exported from the Veeam Agent database per system. The files (.xml) are named after the sever's hostname.

## Notable References
- https://www.veeam.com/windows-linux-availability-agents.html

## Description Of Configuration

The Veeam Agent For Linux Free Edition does not have email notification and therefore makes it difficult to manage and monitor
each server. Email notification has been added by calling a backup script that once configured will in turn run a Veeam Agent job.

Email messages are sent containing the following information.

- job name
- job state
- job configuration
- job session log (attached to email if job failed)

A Veeam Agent job is ran utilizing cron for scheduling under the root user. Two crontab entries are created to run a
incremental backup and then another to run an activefull backup. This process unfortunately is done manually.

**Note:** An activefull backup is done by passing an argument of "full" without double quotes to the backup script.

## Backup Script Installation Instructions For Linux

**Note:** Do not use "veeamconfig ui" to schedule a job.

**Note:** It is assumed the Veeam Agent For Linux has been installed and configured along with exim and mailx.

1. Create the directory to store the backup script and temp files.
   ```
   # mkdir /etc/veeam/scripts
   ```
1. Get the Veeam Agent configuration file and backup script.
    ```
    $ git clone git@gitlab.com:adouglas/veeam-agent-for-linux-backup-script.git
    ```
1. Import applicable Veeam Agent configuration file.
    ```
    # veeamconfig config import --file <hostname-here>.xml
    ```
1. Move the backup script.
    ```
    # mv veeam-backup.sh /etc/veeam/scripts/
    ```
1. Set permissions.
   ```
   # chmod -R 700 /etc/veeam/scripts
   ```
1. Set ownership.
   ```
   $ chown -R root:root /etc/veeam/scripts
   ```
1. Edit backup script settings. JOBNAME and EMAIL should be all that is required to be altered.
   ```
   # nano /etc/veeam/scripts/veeam-backup.sh
   ```
   ```
   # ===================================================
   # = Settings                                        =
   # ===================================================
   JOBNAME="Job-Name-Here"
   SCRIPT_DIR="/etc/veeam/scripts"
   LOG_DIR="/var/log/veeam/Backup/$JOBNAME"
   JOB_INFO=$(veeamconfig job info --name "$JOBNAME")
   EMAIL="systems@example.com"
   # ====================================================
   # = DO NOT EDIT BELOW THIS LINE!                     =
   # ====================================================
   ```
1. Create cronjob entries. Make sure to stagger each backup.
   ```
   # crontab -e
   ```
   ```
   30 23 * * * /etc/veeam/scripts/veeam-backup.sh
   59 23 1 * * /etc/veeam/scripts/veeam-backup.sh full
   ```

## How To...

The instructions are based upon the Veeam Agent For Linux/Windows v3.

### Exporting Configuration File

Linux
```
# veeamconfig config export --file <hostname-here>.xml

```
Windows
```
C:\Program Files\Veeam\EndPoint Backup\Veeam.Agent.Configurator.exe -export /f:C:\Users\user-name-here\Documents\<hostname-here>.xml
```

### Importing Configuration File

Linux
```
# veeamconfig config import --file <hostname-here>.xml
```

Windows
```
C:\Program Files\Veeam\EndPoint Backup\Veeam.Agent.Configurator.exe -import /f:C:\Users\user-name-here\Documents\<hostname-here>.xml
```
